# TP11 - Weather app

## Initialiser le projet

```shell
npm install
```

Compile et hot-reloads pour environnement de developpement

```shell
npm run serve
```

Compile et minification pour environnement de production

```shell
npm run build
```

## Consignes

Vous devez créer une Single Page Application qui affiche la météo du jour. Vous utiliserez le framework Vue.js pour construire votre application et l'API OpenWeather pour les données météorologiques.

Framework CSS interdit

## Contexte du projet

• Un champ de recherche par ville doit permettre d’actualiser le contenu de la page pour afficher le temps dédié

• Vous devez à minima afficher la  température réelle et ressentie, la température minimale et maximale,  la date et bien sûr la ville avec sa latitude et sa longitude

• Des icônes symbolisent les  différents états. Par exemple différents thermomètres avec des couleurs différentes en fonction de la température

• **Bonus** : La vitesse du vent et sa direction

• **Bonus** : Heures du lever et coucher de soleil

• **Bonus** : Géolocalisation au démarrage de l’application