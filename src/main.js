import Vue from 'vue';
import Icon from 'vue-awesome/components/Icon.vue';
import App from './App.vue';

import 'vue-awesome/icons/rainbow';
import 'vue-awesome/icons/sun';
import 'vue-awesome/icons/moon';
import 'vue-awesome/icons/location-arrow';
import 'vue-awesome/icons/search';
import 'vue-awesome/icons/cloud';

Vue.config.productionTip = false;
Vue.component('v-icon', Icon);

new Vue({
  render: (h) => h(App),
}).$mount('#app');
